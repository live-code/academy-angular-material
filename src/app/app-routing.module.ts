import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'demo1', loadChildren: () => import('./features/demo1/demo1.module').then(m => m.Demo1Module) },
  { path: 'demo2', loadChildren: () => import('./features/demo2/demo2.module').then(m => m.Demo2Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
