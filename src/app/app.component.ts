import { Component } from '@angular/core';

@Component({
  selector: 'fb-root',
  template: `

    <hr>

    <div style="position: fixed; top: 10px; right: 40px; z-index: 10; ">
        <mat-icon (click)="drawer.toggle()" style="font-size: 50px">home</mat-icon>
    </div>


    <mat-drawer-container class="example-container" autosize >
      <mat-drawer #drawer class="example-sidenav" mode="side" [opened]="true">
        <mat-list role="list">
          <mat-list-item  routerLink="demo1">Item 1</mat-list-item>
          <mat-list-item  routerLink="demo2">Item 2</mat-list-item>
          <mat-list-item   (click)="drawer.close()">CLOSE</mat-list-item>
        </mat-list>
      </mat-drawer>

      <div class="example-sidenav-content">
        <router-outlet></router-outlet>
      </div>

    </mat-drawer-container>
  `,
  styles: [`
    .example-container {
      width: 100%;
      height: 100vh;
    }

    .example-sidenav-content {
      margin: 10px;
      width: 100%;
      height: 100%;
    }

    .example-sidenav {
      padding: 20px;
    }
  `]
})
export class AppComponent {
  title = 'angular-material-demo';
}
