import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../model/user';
import {delay} from 'rxjs';
import {FormatGbPipe} from '../../shared/pipes/format-gb.pipe';

@Component({
  selector: 'fb-demo1',
  template: `
    <div
      *ngIf="users.length; else loader"
      style=" background-color: red; padding: 10px; display: flex; width: 100%; flex-wrap: wrap;"
    >
      <fb-my-user-card
        *ngFor="let user of users; let i = index"
        [user]="user"
        [index]="i"
        (likeUser)="setUserLikeHandler($event, user.like)"
        (deleteUser)="deleteUser($event)"
      ></fb-my-user-card>
    </div>

    <ng-template #loader>
      <mat-spinner *ngIf="!users.length"></mat-spinner>
    </ng-template>
  `,

  styles: [`
  `]
})
export class Demo1Component  {
  users: User[] = [];

  constructor(private http: HttpClient) {
    http.get<User[]>('http://localhost:3000/users')
      .pipe(delay(2000))
      .subscribe(res => this.users = res)
  }

  setUserLikeHandler(id: number, like: number): void {
    this.http.patch<User>('http://localhost:3000/users/' + id, { like: like + 1})
      .subscribe(res => {
        const index = this.users.findIndex(u => u.id === id)
        this.users[index] = res;
      })
  }

  deleteUser(id: number): void {
    this.http.delete<User[]>('http://localhost3000/users/' + id)
      .subscribe(() => {
        const index = this.users.findIndex(u => u.id === id)
        this.users.splice(index, 1)
      })
  }

}
