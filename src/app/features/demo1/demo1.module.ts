import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo1RoutingModule } from './demo1-routing.module';
import { Demo1Component } from './demo1.component';
import {MyUserCardModule} from '../../shared/layout/my-user-card.module';
import {MatProgressSpinner, MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [
    Demo1Component
  ],
  imports: [
    CommonModule,
    Demo1RoutingModule,
    MatProgressSpinnerModule,
    SharedModule,
  ],
  providers: []
})
export class Demo1Module { }
