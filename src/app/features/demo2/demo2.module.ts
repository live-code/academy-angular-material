import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo2RoutingModule } from './demo2-routing.module';
import { Demo2Component } from './demo2.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MyUserCardModule} from '../../shared/layout/my-user-card.module';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { DialogDeleteComponent } from './components/dialog-delete.component';
import {MatDialogModule} from '@angular/material/dialog';


@NgModule({
  declarations: [
    Demo2Component,
    DialogDeleteComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    MatChipsModule,
    MatIconModule,
    MatListModule,
    DragDropModule,
    MatMenuModule,
    MatIconModule,
    Demo2RoutingModule,
    MyUserCardModule,
    MatButtonModule,
    MatDialogModule
  ]
})
export class Demo2Module { }
