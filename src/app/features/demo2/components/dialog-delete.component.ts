import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'fb-dialog-delete',
  template: `
    <h1 mat-dialog-title>Hi</h1>
    <div mat-dialog-content>
      <p>SIcuro di voler cancellare l'id {{id}}???</p>
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">No Thanks</button>
      <button mat-button [mat-dialog-close]="id" cdkFocusInitial>Ok</button>
    </div>
  `,
  styles: [
  ]
})
export class DialogDeleteComponent implements OnInit {
  id: number | undefined;

  constructor(
    public dialogRef: MatDialogRef<DialogDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { id: number, value: number },
  ) {
    dialogRef.disableClose = true;
    this.id = data.id
    console.log(data.id, data.value)
  }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close()
  }
}
