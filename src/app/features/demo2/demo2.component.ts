import {Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {User} from '../../model/user';
import {HttpClient} from '@angular/common/http';
import {delay} from 'rxjs';
import {MatTab, MatTabChangeEvent} from '@angular/material/tabs';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {MatDialog} from '@angular/material/dialog';
import {DialogDeleteComponent} from './components/dialog-delete.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import gsap from 'gsap';

@Component({
  selector: 'fb-demo2',
  template: `

    <div #list>
      <mat-list role="list"

                cdkDropList class="example-list" (cdkDropListDropped)="drop($event)">
        <mat-list-item
          class="example-box"
          cdkDrag
          *ngFor="let user of users"
        >

            <div>{{user.name}}</div>

            <button mat-icon-button [matMenuTriggerFor]="menu">
              <mat-icon>more_vert</mat-icon>
            </button>

              <mat-menu #menu="matMenu">
                <button mat-menu-item  (click)="deleteUser(user.id)">
                  <mat-icon>delete</mat-icon>
                  <span>Delete</span>
                </button>
                <button mat-menu-item disabled>
                  <mat-icon>edit</mat-icon>
                  <span>edit</span>
                </button>
              </mat-menu>
        </mat-list-item>
      </mat-list>
    </div>

    <mat-tab-group mat-align-tabs="start"  (selectedTabChange)="doSomething($event)">
      <mat-tab
        *ngFor="let user of users" [label]="user.name">
        <fb-my-user-card [user]="user"></fb-my-user-card>
      </mat-tab>
    </mat-tab-group>

    <mat-chip-list>
      <mat-chip *ngFor="let user of users" (removed)="deleteUser(user.id)">
        {{user.name}}
        <button matChipRemove>
          <mat-icon>cancel</mat-icon>
        </button>
      </mat-chip>
    </mat-chip-list>
  `,
  styles: [`
    .example-list {
      width: 500px;
      max-width: 100%;
      border: solid 1px #ccc;
      min-height: 60px;
      display: block;
      background: white;
      border-radius: 4px;
      overflow: hidden;
    }

    .example-box {
      width: 100%;
      padding: 20px 10px;
      border-bottom: solid 1px #ccc;
      color: rgba(0, 0, 0, 0.87);
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      box-sizing: border-box;
      cursor: move;
      background: white;
      font-size: 14px;
    }

    .cdk-drag-preview {
      box-sizing: border-box;
      border-radius: 4px;
      box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),
      0 8px 10px 1px rgba(0, 0, 0, 0.14),
      0 3px 14px 2px rgba(0, 0, 0, 0.12);
    }

    .cdk-drag-placeholder {
      opacity: 1;
    }

    .cdk-drag-animating {
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }

    .example-box:last-child {
      border: none;
    }

    .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }
  `]
})
export class Demo2Component {
  @ViewChildren(MatTab) tabs!: QueryList<MatTab>
  @ViewChild('list') list!: ElementRef<HTMLDivElement>
  users: User[] = [];
  counter = 0;
  index= 0;

  constructor(private http: HttpClient, public dialog: MatDialog, private snackBar: MatSnackBar) {
    http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => {
        this.users = res
        this.index = 2;
      //  gsap.to(this.list.nativeElement, {rotation: 27, x: 100, duration: 1, alpha: 0.2});
      })

    setTimeout(() => {
      console.log(this.tabs.toArray())
      this.tabs.toArray()[0].disabled = true;
      console.log(this.list.nativeElement)
     // gsap.to(this.list.nativeElement, { duration: 1, alpha: 1});
    }, 2000)
  }


  deleteUser(id: number): void {
    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '250px',
      data: {id: id, value: 123},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.http.delete<User[]>('http://localhost:3000/users/' + result)
          .subscribe(() => {
            const index = this.users.findIndex(u => u.id === result)
            this.users.splice(index, 1)
            this.snackBar.open('item deleted', 'DONE');
          })
      }
    });
  }

  doSomething(event: MatTabChangeEvent) {
    console.log('tab changed', event)
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.users, event.previousIndex, event.currentIndex);
    const ids = this.users.map(u => u.id)
    console.log(ids)
    // ... scrivere su server ...
  }
}
