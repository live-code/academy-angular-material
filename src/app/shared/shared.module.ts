import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MyUserCardModule} from './layout/my-user-card.module';
import { FormatGbPipe } from './pipes/format-gb.pipe';
import {FormatGbModule} from './pipes/format-gb.module';

@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    MyUserCardModule,
    FormatGbModule
  ],
  exports: [
    FormatGbModule,
    MyUserCardModule
  ]
})
export class SharedModule { }
