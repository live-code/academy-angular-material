import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormatGbPipe} from './format-gb.pipe';



@NgModule({
  declarations: [
    FormatGbPipe
  ],
  exports: [
    FormatGbPipe
  ],
  imports: [
    CommonModule
  ]
})
export class FormatGbModule { }
