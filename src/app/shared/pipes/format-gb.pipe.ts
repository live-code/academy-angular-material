import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatGb'
})
export class FormatGbPipe implements PipeTransform {

  transform(value: number): string {
    return `${value / 1000} Gb`;
  }

}
