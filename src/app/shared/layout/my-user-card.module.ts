import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MyUserCardComponent} from './my-user-card.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {SharedModule} from '../shared.module';
import {FormatGbModule} from '../pipes/format-gb.module';



@NgModule({
  declarations: [
    MyUserCardComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatCardModule,
    FormatGbModule
  ],
  exports: [
    MyUserCardComponent
  ]
})
export class MyUserCardModule { }
