import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../model/user';
import {animate, animation, state, style, transition, trigger} from '@angular/animations';

export const animations =  [
  trigger('visibility', [
    state('show', style({
      opacity: 1,
    })),
    state('hide', style({
      opacity: 0,
    })),
    transition('show <=> hide', [
      animate('1s')
    ]),
  ]),
  trigger('toggle', [
    state('open', style({
      height: '*',
    })),
    state('close', style({
      height: 0,
    })),
    transition('open <=> close', [
      animate('0.6s ease-in-out')
    ]),
  ]),

]
@Component({
  selector: 'fb-my-user-card',
  animations: animations,
  template: `

    <mat-card
      [@visibility]="showLayout ? 'show' : 'hide'"
      *ngIf="user"
      class="example-card" style="margin: 10px">
      <mat-card-header (click)="isOpened = !isOpened">
        <div mat-card-avatar class="example-header-image"></div>
        <mat-card-title>{{user.name}}</mat-card-title>
        <mat-card-subtitle>{{user.mb | formatGb}}</mat-card-subtitle>
      </mat-card-header>
      <div [@toggle]="isOpened ? 'open' : 'close'" style="overflow: hidden">
        <img mat-card-image src="https://material.angular.io/assets/img/examples/shiba2.jpg" alt="Photo of a Shiba Inu">
        <mat-card-content>
          {{user.company.name}} <br>
          {{user.phone}} <br>

        </mat-card-content>
        <mat-card-actions>
          <button mat-button (click)="likeUser.emit(user.id)">LIKE ({{user.like}})</button>
          <button mat-button (click)="deleteUser.emit(user.id)">DELETE</button>
        </mat-card-actions>
      </div>
    </mat-card>
  `,
  styles: [`
    .example-card {
      max-width: 300px;
      opacity: 0;
    }

    .example-header-image {
      background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');
      background-size: cover;
    }
  `]
})
export class MyUserCardComponent implements AfterViewInit {
  @Input() user: User | undefined
  @Input() index: number = 0;
  @Output() deleteUser = new EventEmitter<number>();
  @Output() likeUser = new EventEmitter<number>();

  isOpened = false;
  showLayout = false;

  ngAfterViewInit() {
    setTimeout(() => {
      this.showLayout = true;
    }, 400 * this.index)
  }

}
